
/*
    TODO:
    - SCSS: Good column mixin
    - SCSS: Variables in objects??
*/

const gulp                  = require('gulp');
const environments          = require('gulp-environments');
const runSequence           = require('run-sequence');
const del                   = require('del');
const multiDest             = require('gulp-multi-dest');
const source                = require('vinyl-source-stream');
const buffer                = require('vinyl-buffer');
const sass                  = require('gulp-sass');
const cleanCSS              = require('gulp-clean-css');
const postcss               = require('gulp-postcss');
const sourcemaps            = require('gulp-sourcemaps');
const autoprefixer          = require('autoprefixer');
const concat                = require('gulp-concat');
const uglify                = require('gulp-uglify');
const rename                = require('gulp-rename');
const browserify            = require('browserify');
const vueify                = require('vueify');
const size                  = require('gulp-size');
const gutil                 = require('gulp-util');
const beep                  = require('beepbeep');
const cheerio               = require('gulp-cheerio');                  // https://github.com/knpwrs/gulp-cheerio
const cssBase64             = require('gulp-css-base64');               // https://devhub.io/repos/Wenqer-gulp-base64
const eslint                = require('gulp-eslint');                   
const argv                  = require('yargs').argv;                    // https://www.npmjs.com/package/yargs
const path                  = require('path');
const notifier              = require('node-notifier');
const svgmin                = require('gulp-svgmin');                   // https://github.com/ben-eb/gulp-svgmin
const svgstore              = require('gulp-svgstore');                 // https://github.com/w0rm/gulp-svgstore
const imagemin              = require('gulp-imagemin');                 // https://www.npmjs.com/package/gulp-imagemin

const development           = environments.development;
const production            = environments.production;

const config                = {};


// Log errors to console
const logError = function(err) {
    if (development()) { beep(); }
    gutil.log('[Error]', err.message);
    this.emit('end');
};

// Throw a party!
const getCompliment = function() {
    const compliments = ['Great work!'];
    return compliments[Math.floor(compliments.length*Math.random())];
};

// Notifications in OS
const doNotify = (title, message) => notifier.notify({ 
    title: title, 
    message: message
});

// Config paths
config.paths = {
    dest:           './dist',
    //appDest: (argv.destpath) ? argv.destpath :  '../../../application/website/static',
    site: {
        css:        ['./src/scss/styles.scss'], // CSS entry points
        js:         ['./src/js/scripts.js'], // JS entry points
        images:     './src/img',
        fonts:      './src/font',
        icons:      './src/img/icons'
    },
    html:           './src/html',
    eslint:         [
                        './src/js/**/*.js', 
                        './src/js/**/*.vue', 
                        '!./src/js/Vendor/**/*.js'
                    ],
    watch: {
        css:        ['./src/scss/**/*.scss'],
        js:         ['./src/js/**/*.js', './src/js/**/*.vue'],
        html:       ['./src/html/**/*.*'],
        images:     [
                        './src/img/**/*.jpg', 
                        './src/img/**/*.png', 
                        './src/img/**/*.gif', 
                        './src/img/**/*.webp'
                    ],
        svg:        [
                        './src/img/**/*.svg',
                        '!./src/img/icons/_map.svg'
                    ],
        icons:      [
                        './src/img/icons/**/*.svg',
                        '!./src/img/icons/_map.svg'
                    ],
        fonts:      ['./src/font/**/*.*']
    }
};

// Config settings
config.settings = {
    cssBase64: {
        baseDir: './',
        extensionsAllowed: ['.svg', '.png'],
        maxWeightResource: 8*1024*10, // 10KB
        deleteAfterEncoding: false,
        debug: true
    },
    svgmin: {
        plugins: [
            { cleanupAttrs: true },
            { removeDoctype: true },
            { removeXMLProcInst: true },
            { removeComments: true },
            { removeMetadata: true },
            { removeTitle: true },
            { removeDesc: true },
            { removeUselessDefs: true },
            { removeXMLNS: true },
            { removeEditorsNSData: true },
            { removeEmptyAttrs: true },
            { removeHiddenElems: true },
            { removeEmptyText: true },
            { removeEmptyContainers: true },
            { removeViewBox: false },
            { cleanupEnableBackground: true },
            { minifyStyles: false },
            { convertStyleToAttrs: true },
            { convertColors: { 
                currentColor: false, 
                rgb2hex: false 
            } },
            { convertPathData: false },
            { convertTransform: true },
            { removeUnknownsAndDefaults: {
                keepDataAttrs: false
            } },
            { removeAttrs: {
                attrs: ['overflow', 'baseProfile', 'version']
            } },
            { removeNonInheritableGroupAttrs: false },
            { removeUselessStrokeAndFill: true },
            { removeUnusedNS: true },
            { cleanupIDs: false }, // !
            { cleanupNumericValues: { 
                floatPrecision: 4, 
                leadingZero: true 
            } },
            { cleanupListOfValues: {
                floatPrecision: 4, 
                leadingZero: true 
            } },
            { moveElemsAttrsToGroup: true },
            { moveGroupAttrsToElems: false },
            { collapseGroups: true },
            { removeRasterImages: false },
            { mergePaths: false },
            { convertShapeToPath: false },
            { sortAttrs: false },
            { transformsWithOnePath: false },
            { removeDimensions: false },
            { removeElementsByAttr: false },
            { addClassesToSVGElement: false },
            { addAttributesToSVGElement: false },
            { removeStyleElement: true },
            { removeScriptElement: true }
        ]
    },
    imagemin: [
        imagemin.gifsicle({
            optimizationLevel: 2,
            interlaced: true
        }),
        imagemin.jpegtran({
            progressive: true
        }),
        imagemin.optipng({
            optimizationLevel: 5,
            bitDepthReduction: true,
            colorTypeReduction: true,
            paletteReduction: true
        })
    ],
    multiDest: {
        mode: '0755'
    }
};

// Add styleguide utility scss and js to paths when develop env
if (development()) { 
    config.paths.site.css.push('./src/scss/styleguide.scss');    
    config.paths.site.js.push('./src/js/styleguide.js');   
}



// Default (development) task
gulp.task('default', () => {
    runSequence(
        'clean', 
        'assets', 
        [ 
            'site-css', 
            'site-js',
            'move-html',
            'move-fonts'
        ], 
        () => doNotify('Default task complete', getCompliment())
    );
});

// Watch files
gulp.task('watch', () => {    
    gutil.log('   [Log]', 'Starting watch');

    // JS
    gulp.watch(config.paths.watch.js, () => runSequence(
        ['site-js'], 
        () => doNotify('JS processed', getCompliment())
    )); 

    // CSS
    gulp.watch(config.paths.watch.css, () => runSequence(
        'site-css',
        () => doNotify('CSS processed', getCompliment())
    ));   

    // HTML    
    gulp.watch(config.paths.watch.html, () => runSequence(
        'move-html', 
        () => doNotify('HTML processed', getCompliment())
    ));    

    // Fonts     
    gulp.watch(config.paths.watch.font, () => runSequence(
        'move-fonts',
        () => doNotify('Fonts processed', getCompliment())
    )); 

    // Images (bitmaps)   
    gulp.watch(config.paths.watch.images, () => runSequence(
        'optimize-images', 
        () => doNotify('Images processed', getCompliment())
    ));  

    // SVG images         
    gulp.watch(config.paths.watch.svg, () => runSequence(
        'optimize-svg', 
        () => doNotify('SVG processed', getCompliment())
    ));  

    // Icons     
    gulp.watch(config.paths.watch.icons, () => runSequence(
        'svg-map', 
        () => doNotify('SVG map processed', getCompliment())
    ));  
});

// Build (production) task
gulp.task('build', () => {
    gutil.log('   [Log]', 'Building for ' + (development() ? 'Development' : 'Production'));
    runSequence(
        'clean', 
        'assets', 
        [
            'site-css', 
            'site-js', 
            'move-fonts'
        ]
    );
});





// ESlinting check, manually run
gulp.task('lint', () => {   
    return gulp.src(config.paths.eslint)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

// Clean dest
gulp.task('clean', () => {
    return del(config.paths.dest);
});

// Clean appDest
//gulp.task('cleanApp', () => {
//    return del(config.paths.appDest);
//});

// Move dest to appDest
//gulp.task('moveApp', () => {
//    gutil.log('   [Log]', 'Moving static files to ' + config.paths.appDest);
//    return gulp.src(config.paths.dest + '/**/*')
//    .pipe(gulp.dest(config.paths.appDest));
//});

// Parse CSS
const parseCss = (paths, outputFilename) => {
    return gulp.src(paths)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', logError))    
    .pipe(concat(outputFilename))
    .pipe(cssBase64(config.settings.cssBase64)) // Use an absolute path (ie. /dist/img/foo.png) in SCSS to use the optimised images in dest
    .pipe(postcss([         
        autoprefixer({ 
            browsers: [
                'ie >= 10',
                'iOS >= 7',
                'Firefox > 20'
            ],
            supports: false,
            remove: false
        }) 
    ]))   
    .pipe(production(cleanCSS({
        inline: ['local']
    })))
    .pipe(development(size({
        title: '   [CSS]',
        pretty: true,
        showFiles: true,
        showTotal: false
    })))
    .pipe(sourcemaps.write('./srcmap'))
    .pipe(gulp.dest(config.paths.dest + '/css')); // To dest
};

// Parse JS 
const parseJs = (paths, outputFilename) => {    
    return browserify({
        entries: paths,
        cache: {},
        debug: development(),
        packageCache: {}
    })
    .transform('vueify')
    .transform('babelify')
    .bundle()
    .on('error', logError)
    .pipe(source(outputFilename))
    .pipe(buffer())
    .pipe(sourcemaps.init({
        loadMaps: true
    }))
    .pipe(production(uglify()))
    .pipe(development(size({
        title: '    [JS]',
        pretty: true,
        showFiles: true,
        showTotal: false
    })))
    .pipe(sourcemaps.write('./srcmap'))
    .pipe(gulp.dest(config.paths.dest + '/js')); // To dest
};




// Site CSS
gulp.task('site-css', () => {
    return parseCss(config.paths.site.css, 'styles.css');
});

// Site JS 
gulp.task('site-js', () => {    
    return parseJs(config.paths.site.js, 'scripts.js');
});



// Copy static html files 
gulp.task('move-html', () => {   
    return gulp.src(config.paths.watch.html)
    .pipe(gulp.dest(config.paths.dest + '/html')); // To dest
});



// Copy font files 
gulp.task('move-fonts', () => {   
    return gulp.src(config.paths.watch.fonts)
    .pipe(gulp.dest(config.paths.dest + '/font')); // To dest
});



// Process assets
gulp.task('assets', () => {
    runSequence(
        'move-fonts',
        'optimize-images',
        'optimize-svg',
        'svg-map'
    );
});

// Copy images
//gulp.task('move-images', () => {   
//    return gulp.src(config.paths.watch.images)
//    .pipe(gulp.dest(config.paths.dest + '/img')); // To dest
//});

// Copy fonts
gulp.task('move-fonts', () => {   
    return gulp.src(config.paths.watch.fonts)
    .pipe(gulp.dest(config.paths.dest + '/font')); // To dest
});

// Optimize bitmap images
gulp.task('optimize-images', () => {
    return gulp.src(config.paths.watch.images)
    .pipe(imagemin(config.settings.imagemin))
    .pipe(development(size({
        title: '   [IMG]',
        pretty: true,
        showFiles: true,
        showTotal: false
    })))
    .pipe(gulp.dest(config.paths.dest + '/img')); // To dest
});

// Optimize all SVG sources
gulp.task('optimize-svg', () => {
    return gulp.src(config.paths.watch.svg)
    .pipe(svgmin(config.settings.svgmin))
    .pipe(development(size({
        title: '   [SVG]',
        pretty: true,
        showFiles: true,
        showTotal: false
    })))
    .pipe(gulp.dest(config.paths.dest + '/img')); // To dest
});

// Create SVG icon symbol map
gulp.task('svg-map', () => {
    return gulp.src(config.paths.watch.icons)
    .pipe(svgmin(config.settings.svgmin)) // Optimize SVG icon sources before inclusion
    .pipe(rename({ 
        prefix: 'icon-' 
    }))
    .pipe(svgstore())
    .pipe(svgmin(config.settings.svgmin)) // Optimize resulting SVG symbol map
    .pipe(rename('_map.svg'))
    .pipe(development(size({
        title: '   [Map]',
        pretty: true,
        showFiles: true,
        showTotal: false
    })))
    .pipe(multiDest([
        config.paths.site.icons, // To source directory, for under version control
        config.paths.dest + '/img/icons' // A copy in dest
    ], config.settings.multiDest));
});


