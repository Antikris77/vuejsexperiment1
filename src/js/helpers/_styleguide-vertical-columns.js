
class SGUIVerticalColumns {
    constructor() {
        this._init();
    }

    _init() {
        const html = document.querySelector('html');
        const body = document.querySelector('body');

        this._overlay = document.createElement('div');
        this._overlay.id = 'sgui-column-overlay';
        this._overlay.className = 'is-hidden';
        this._overlay.innerHTML= '<div id="sgui-column-overlay--columns"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';	
        
        body.appendChild(this._overlay);
        body.addEventListener(
            'keyup', 
            event => this._toggle(event)
        );
    }

    _toggle(event) {
        if (event.keyCode && event.keyCode === 112 && event.altKey) { // Alt F1
            if (this._overlay.className === 'is-hidden') {
                this._overlay.className = '';
            } else {
                this._overlay.className = 'is-hidden';
            }
        }
    }
}

const init = () => {
    const sguiVerticalColumns = new SGUIVerticalColumns();
};

document.addEventListener('DOMContentLoaded', init, { once: true });
