
class SGUIHorizontalGrid {
    constructor(space = 32) {
        this._space = space; // default pattern height, can be overwritten via CSS
        this._isLocked = true;
	    this._isVisible = false;

        this._init();
    }

    _init() {
        this._svg = null;
        
	    this._overlay = document.createElement('div');
        this._overlay.id = 'sgui-horgrid-overlay';
        this._overlay.className = 'is-hidden';
        document.body.appendChild(this._overlay);
        
	    this._sample = document.createElement('div');
        this._sample.id = 'sgui-horgrid-sample';
        document.body.appendChild(this._sample);

        this._update();

        document.body.addEventListener(
            'keyup',  
            event => this._toggle(event)
        );
        document.body.addEventListener(
            'click',  
            event => this._lock(event)
        );

        let timer;
        const resizeHandler = () => {
            clearTimeout(timer);
            timer = setTimeout(() => this._update(), 50);
        };
        
        window.addEventListener(
            'resize', 
            resizeHandler
        );
    }

    _update() {
		let sH = this._sample.offsetHeight;
		let h = (sH > 0 ? sH : false) || this._space; // todo `
		let pattern = `<svg xmlns="http://www.w3.org/2000/svg" shape-rendering="crispEdges" x="0" y="0" width="${ h }" height="${ h }"><line stroke="#8080ff" stroke-width="1" x1="0" x2="${ h }" y1="0" y2="0" /><line stroke="#8080ff" stroke-opacity="0.25" stroke-width="1" x1="0" x2="${ h }" y1="${ h * 0.25 }" y2="${ h * 0.25 }" /><line stroke="#8080ff" stroke-opacity="0.5" stroke-width="1" x1="0" x2="${ h }" y1="${ h * 0.5 }" y2="${ h * 0.5 }" /><line stroke="#8080ff" stroke-opacity="0.25" stroke-width="1" x1="0" x2="${ h }" y1="${ h * 0.75 }" y2="${ h * 0.75 }" /></svg>`;

		this._overlay.style.height = document.body.scrollHeight + 'px';
		let svg = `<svg xmlns="http://www.w3.org/2000/svg" x="0" y="0"><defs><pattern id="sgui-vertical" width="${ h }" height="${ h }" patternUnits="userSpaceOnUse"><image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="data:image/svg+xml;base64,${ window.btoa(pattern) }" x="0" y="0" width="${ h }" height="${ h }"></image></pattern></defs><rect fill="url(#sgui-vertical)" width="100%" height="100%" /></svg>`;
		this._overlay.innerHTML = svg;
        this._svg = this._overlay.querySelector('svg');

        this._getYFromCookie();
    }

    _getYFromCookie() {        
        let y = parseInt(document.cookie.replace(/(?:(?:^|.*;\s*)sguiHorizontalGridY\s*=\s*([^;]*).*$)|^.*$/, '$1'), 10);
        if (!isNaN(y)) {
            this._setY(y);
        }
    }

    _setYToCookie(y) {
        document.cookie = 'sguiHorizontalGridY=' + y;
    }

    _trackY(event) {
        if (this._isLocked === false) {
			this._setY(event.clientY);
		}
	}
    
    _setY(y) {
        this._svg.style.top = y + 'px';
    }

    _toggle(event) {
		if (event.keyCode && event.keyCode === 113 && event.altKey) { // Alt F2
			this._update();
			this._setY(event.clientY);
        
            const track = e => this._trackY(e);

			if (this._isVisible) {
				document.body.removeEventListener(
                    'mousemove', 
                    track, 
                    { passive: true }
                );
                this._overlay.classList.add('is-hidden');
				this._isVisible = false;
			} else {
				document.body.addEventListener(
                    'mousemove', 
                    track, 
                    { passive: true }
                );
                this._overlay.classList.remove('is-hidden');
				this._isVisible = true;
			}
		}
    }

    _lock(event) {
		if (this._isVisible) {	
			let y;	
			if (this._isLocked === true) {
				y = event.clientY; 
                this._overlay.classList.remove('is-locked');
				this._isLocked = false;
			} else {
				y = document.documentElement.scrollTop + event.clientY;
                this._setYToCookie(y);
                this._overlay.classList.add('is-locked');
				this._isLocked = true;
			}
			this._setY(y);
		}
    }
}

const init = () => {
    const sguiHorizontalGrid = new SGUIHorizontalGrid(32);
};

document.addEventListener('DOMContentLoaded', init, {once: true });
