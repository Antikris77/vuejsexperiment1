
import clonify from '../functions/clonify';

export const setActiveFilter = (context, { id, value }) => {
    context.commit('setActiveFilter', { id, value });
    
    // Request the value, returns promise
    //context.dispatch('getActiveFilterValue', { id }).then(
    //    (result) => console.log('post-addActiveFilter', id, result)
    //);    
};

export const resetActiveFilters = (context) => {
    const value = clonify(context.getters.defaultActiveFilters);
    context.commit('resetActiveFilters', { value });
};

export const addWishlistItem = (context, { id, amount }) => {
    context.commit('addWishlistItem', { id, amount });
};

export const removeWishlistItem = (context, { id }) => {
    context.commit('removeWishlistItem', { id });
};

export const updateWishlistItem = (context, { id, props }) => {
    context.commit('updateWishlistItem', { id, props });
};

export const addCartItem = (context, { id, amount }) => {
    context.commit('addCartItem', { id, amount });
};

export const removeCartItem = (context, { id }) => {
    context.commit('removeCartItem', { id });
};

export const updateCartItem = (context, { id, props }) => {
    context.commit('updateCartItem', { id, props });
};

export const addNotification = (context, { note }) => {
    context.commit('addNotification', { note });
};

export const removeNotification = (context, { id }) => {
    context.commit('removeNotification', { id });
};

export const storeDraggedProduct = (context, { product }) => {
    context.commit('setDraggedProduct', { product });
};

