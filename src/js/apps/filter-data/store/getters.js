
export const appSettings = state => {
    return state.settings;
};

export const definitions = state => {
    return state.definitions;
};

export const filterTemplateData = state => {
    return state.filterTemplateData;
};

export const activeFilters = (state) => {
    return state.activeFilters;
};

export const defaultActiveFilters = (state) => {
    return state.defaultActiveFilters;
};

export const sortingTemplateData = (state) => {
    return state.sortingTemplateData;
};

export const products = (state) => {
    return state.products;
};

export const wishlist = (state) => {
    return state.wishlist;
};

export const cart = (state) => {
    return state.cart;
};

export const notifications = (state) => {
    return state.notifications;
};

export const draggedProduct = (state) => {
    return state.draggedProduct;
};

