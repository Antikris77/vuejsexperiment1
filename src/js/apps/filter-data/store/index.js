
import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';
import * as actions from './actions';
import * as mutations from './mutations';

import tagTemplate from '../functions/tag-template.js'; 

Vue.use(Vuex);

const state = {
    activeFilters: [],
    wishlist: [],
    cart: [],
    notifications: [],
    defaultActiveFilters: [],
    draggedProduct: null
};

// App settings
state.settings = {
    language: 'nl',
    notifications: {
        livetime: 3500
    }
};

// Textual definitions
state.definitions = {
    interface: [
        { def: 'Catalogus',             id: 'catalogue' },
        { def: 'Winkelwagen',           id: 'cart' },
        { def: 'Verlanglijstje',        id: 'wishlist' },
        { def: 'Elke',                  id: 'anyOption' }, 
        { def: 'Reset',                 id: 'resetButton' },
        { def: 'In winkelwagen',        id: 'addToCartButton' },
        { def: 'Op verlanglijstje',     id: 'addToWishlistButton' },
        { def: 'Naar winkelwagen',      id: 'moveToCartButton' },
        { def: 'Naar verlanglijstje',   id: 'moveToWishlistButton' },
        { def: 'Uit winkelwagen',       id: 'removeFromCartButton' },
        { def: 'Uit verlanglijstje',    id: 'removeFromWishlistButton' },
        { def: 'Totaal',                id: 'totalAmount' },
        { 
            def: 'Geen producten gevonden.', 
            id: 'noProductsFound' 
        }      
    ],
    notifications: [
        // How in the hell can I ever get the following in a JSON response from an API request?
        { 
            def: tagTemplate `<em>${0}</em> is <em class="created">toegevoegd</em> aan uw <em>winkelwagen</em>.`,
            id: 'addToCart' 
        },
        { 
            def: tagTemplate `<em>${0}</em> is <em class="created">toegevoegd</em> aan uw <em>verlanglijstje</em>.`,
            id: 'addToWishlist' 
        },
        { 
            def: tagTemplate `<em>${0}</em> is <em class="created">verplaatst</em> naar uw <em>winkelwagen</em>.`,
            id: 'moveToCart' 
        },
        { 
            def: tagTemplate `<em>${0}</em> is <em class="created">verplaatst</em> naar uw <em>verlanglijstje</em>.`,
            id: 'moveToWishlist' 
        },
        { 
            def: tagTemplate `<em>${0}</em> is <em class="destroyed">verwijderd</em> uit uw <em>winkelwagen</em>.`,
            id: 'removeFromCart' 
        },
        { 
            def: tagTemplate `<em>${0}</em> is <em class="destroyed">verwijderd</em> uit uw <em>verlanglijstje</em>.`,
            id: 'removeFromWishlist' 
        }
    ],
    confirmations: [
        {
            def: tagTemplate `Weet u zeker dat u "${0}" uit uw winkelwagen wilt verwijderen?`,
            id: 'removeFromCart'
        },
        {
            def: tagTemplate `Weet u zeker dat u "${0}" van uw verlanglijstje wilt verwijderen?`,
            id: 'removeFromWishlist'
        }
    ],
    product: [
        { 
            def: 'Soort',
            id: 'type',
            children:  [
                { def: 'Ballpoint',         id: 'pen' },
                { def: 'Potlood',           id: 'pnc' },
                { def: 'Felt marker',       id: 'mrk' },
                { def: 'Kroontjespen',      id: 'rfp' },
                { def: 'Finewriter',        id: 'fwr' },
            ]
        },
        {
            def: 'Kleur',
            id: 'color',
            children: [
                { def: 'Zwart',             id: 'blk' },
                { def: 'Rood',              id: 'red' },
                { def: 'Blauw',             id: 'blu' },
                { def: 'Groen',             id: 'grn' },
                { def: 'Geel',              id: 'ylw' },
                { def: 'Oranje',            id: 'ora' },
                { def: 'Paars',             id: 'prp' },
                { def: 'Zilver',            id: 'slv' }
            ]
        },
        {
            def: 'Eigenschappen',
            id: 'feats',
            children: [            
                { def: 'Watervast',         id: 'wrs' },
                { def: 'Ergonomisch',       id: 'erg' },
                { def: 'Hervulbaar',        id: 'rfi' },
                { def: 'Niet giftig',       id: 'tox' },
                { def: 'Hercyclebaar',      id: 'rcy' }
            ]
        },
        {
            def: 'Kan op voorraad geleverd worden',
            id: 'avail'
        }
    ]
};

// Filter options template data
state.filterTemplateData = [
    { 
        id: 'type',
        type: 'singleOption',
        appliesTo: 'type',
        options: ['pen', 'pnc', 'mrk', 'rfp', 'fwr']
    }, 
    { 
        id: 'color',
        type: 'singleOption',
        appliesTo: 'colors',
        options: ['blk', 'red', 'blu', 'grn', 'ylw', 'ora', 'prp', 'slv']
    }, 
    { 
        id: 'feats',
        type: 'multiOption',
        appliesTo: 'feats',
        options: ['wrs', 'erg', 'rfi', 'tox', 'rcy']
    }, 
    { 
        id: 'avail',
        type: 'booleanOption',
        appliesTo: 'avail'
    }
];

// Sorting options template data
state.sortingTemplateData = [
    {
        name: 'Product', // TODO: put name of sorters in definitions
        id: 'sortProductName',
        method: function (a, b) { 
            return a.name.localeCompare(b.name); 
        }
    },
    {
        name: 'Prijs',
        id: 'sortProductPrice',
        method: function (a, b) { 
            return a.price.value - b.price.value; 
        }
    }
];

// Product data
state.products = [    
    {
        name: 'Parker Ballpoint Premium, Zwart',
        uri: 'http://google.com',
        id: '328392842',
        description: 'Amet, consectetur adipiscing elit. Amet auctor quam. Volutpat eu pellentesque eget, pretium a lorem. Amet, consectetur adipiscing elit. Amet auctor quam. Volutpat eu pellentesque eget, pretium a lorem. Amet, consectetur adipiscing elit. Amet auctor quam. Volutpat eu pellentesque eget, pretium a lorem.',
        thumbnail: '../img/products/th-328392842.jpg',
        type: ['pen'],
        colors: ['blk', 'red', 'ylw'],
        feats: ['tox', 'erg', 'rfi'],
        avail: true,
        price: {
            value: 1.25,
            currency: 'EUR'
        }
    },
    {
        name: 'BIC Ballpoint, Blauw',
        uri: 'http://google.com',
        id: '298309943',
        description: 'Amet, consectetur adipiscing elit. Volutpat eu pellentesque eget, pretium a lorem. Amet, consectetur adipiscing elit. Volutpat eu pellentesque eget, pretium a lorem. Amet, consectetur adipiscing elit. Volutpat eu pellentesque eget, pretium a lorem.',
        thumbnail: '../img/products/th-298309943.jpg',
        type: ['pen'],
        colors: ['blu'],
        feats: ['tox', 'rcy'],
        avail: true,
        price: {
            value: 1.25,
            currency: 'EUR'
        }
    },
    {
        name: 'Vistaprint Pen, Zilver',
        uri: 'http://google.com',
        id: '20930293',
        description: 'Lorem ipsum dolor sit amet, sit amet auctor quam. Pellentesque eget, pretium a lorem. Lorem ipsum dolor sit amet, sit amet auctor quam. Pellentesque eget, pretium a lorem. Lorem ipsum dolor sit amet, sit amet auctor quam. Pellentesque eget, pretium a lorem.',
        thumbnail: '../img/products/th-20930293.jpg',
        type: ['pen'],
        colors: ['slv'],
        feats: ['tox', 'erg', 'rfi'],
        avail: true,
        price: {
            value: 6.30,
            currency: 'EUR'
        }
    },
    {
        name: 'Mont Blanc Meisterstuck Gold Coated Classique, Zwart',
        uri: 'http://google.com',
        id: '29832109382',
        description: 'Consectetur adipiscing elit. Pellentesque sit amet auctor quam.Pellentesque eget, pretium a lorem. Consectetur adipiscing elit. Pellentesque sit amet auctor quam.Pellentesque eget, pretium a lorem. Consectetur adipiscing elit. Pellentesque sit amet auctor quam.Pellentesque eget, pretium a lorem.',
        thumbnail: '../img/products/th-29832109382.jpg',
        type: ['pen'],
        colors: ['blk'],
        feats: ['wrs', 'erg', 'rfi'],
        avail: false,
        price: {
            value: 26.85,
            currency: 'EUR'
        }
    }
];

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
});

/*
    TODO:

        - More filter types:
            - Value range (min, max)
            - Property text search (free text applied to for instance a title) - [Property...] [Contains|Does not contain] [Free text]
            - Date (date selection, applied to any date property) - ...
        - Centralization of naming schemes. Language support.
        - Wishlist priority property
        - Undo option in notifications?
        - Filters <-> data binding in order to have multiple sections of filtering/sorting going (applying perhaps even to different products)
        - Currency conversion and (localized) display
        - Save filtering and sorting
        - Compare products
        - Drag and drop products
        - 'Detail page'
        - More products
        - Routing between 'pages'
        - Continuous scrolling in products lists
        - Product display: list feats, colors, etc. but behind a collapsible, truncate description
        - Notifications of various kind (color)

        - Put it all under versioning control


*/

