
import createId from '../functions/create-id.js';

/**
 * Add or modify an active filter to state.activeFilters. In case an undefined value is passed, the filter object is removed from the state.activeFilters.
 * 
 * @param {any} { id, value } - id: ID string of a filter. value: value of any type that applies to the filter (i.e. an array for multiOptions) 
 */
export const setActiveFilter = (state, { id, value }) => {
    const item = state.activeFilters.find(f => f.id === id);

    if (item) {
        if (value && value !== '' && value.length !== 0) {
            item.value = value;
        } else {
            const i = state.activeFilters.indexOf(item);
            state.activeFilters.splice(i, 1);            
        }
    } else {
        state.activeFilters.push({ id, value });
    }
};

/**
 * Reset all content in state.activeFilters to a desired array; here we usually pass it state.defaultActiveFilters, but it can also be a stored user custom setup.
 * 
 * @param {any} { value } - value: an array of objects
 */
export const resetActiveFilters = (state, { value }) => {
    state.activeFilters = value;
};

/**
 * Adds a product reference object to state.wishlist. In case an object with the same id value is already present, its amount is increased.
 * The reference object has the format of { id: productID, amount: integer }.
 * 
 * @param {any} { id } - id: the ID string of a product. amount: the amount of items to add
 */
export const addWishlistItem = (state, { id, amount=1 }) => {
    const item = state.wishlist.find(i => i.id === id);
    if (item) {
        item.amount += amount;
    } else {
        state.wishlist.push({ id, amount });
    }
};

/**
 * Removes a product reference object from state.wishlist.
 * 
 * @param {any} { id } - id: the ID string of a product
 */
export const removeWishlistItem = (state, { id }) => {
    const item = state.wishlist.find(i => i.id === id);
    if (item) {
        const index = state.wishlist.indexOf(item);
        state.wishlist.splice(index, 1);
    }
};

export const updateWishlistItem = (state, { id, props={} }) => {
    const item = state.wishlist.find(i => i.id === id);
    if (item) {
        item.amount = props.amount || item.amount;
    }
};

/**
 * Adds a product reference object to state.cart. In case an object with the same id value is already present, its amount is increased.
 * The reference object has the format of { id: productID, amount: integer }.
 * 
 * @param {any} { id } - id: the ID string of a product. amount: the amount of items to add
 */
export const addCartItem = (state, { id, amount=1 }) => {
    const item = state.cart.find(i => i.id === id);
    if (item) {
        item.amount += amount;
    } else {
        state.cart.push({ id, amount });
    }
};

/**
 * Removes a product reference object from state.cart.
 * 
 * @param {any} { id } - id: the ID string of a product
 */
export const removeCartItem = (state, { id }) => {
    const item = state.cart.find(i => i.id === id);
    if (item) {
        const index = state.cart.indexOf(item);
        state.cart.splice(index, 1);
    }
};

export const updateCartItem = (state, { id, props={} }) => {
    const item = state.cart.find(i => i.id === id);
    if (item) {
        item.amount = props.amount || item.amount;
    }
};

export const addNotification = (state, { note }) => {
    note.active = true;
    note.created = new Date();
    note.id = createId();
    state.notifications.push(note);
};

export const removeNotification = (state, { id }) => {
    // Instead of actually removing the note from the store (which causes problems with generated emits and rerenders?), we disable it
    const storedNote = state.notifications.find((item) => item.id === id);
    storedNote.active = false;
};

export const purgeNotications = (state) => {
    state.notifications = [];
};

export const setDraggedProduct = (state, { product }) => {
    state.draggedProduct = product;
};

