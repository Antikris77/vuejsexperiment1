
import Vue from 'vue';
import { mapGetters } from 'vuex';

// Global mixin to return definition strings from the store
// Not sure I'm doing this right, though...
Vue.mixin({
    computed: {
        ...mapGetters(['definitions'])
    },
    methods: { 
        def({ set, kind, id }) {
            const s = this.definitions[set];
            if (kind) {
                const k = s.find(i => i.id === kind);
                return k.children.find(i => i.id === id).def;
            }    
            return s.find(i => i.id === id).def;   
        }
    }
});
