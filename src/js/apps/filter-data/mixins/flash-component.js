
import Vue from 'vue';

Vue.mixin({
    data () {
        return {
            flash: false
        };
    },
    methods: {        
        flashComponent () {
            this.flash = false;
            window.setTimeout(() => this.flash = true, 1);
            window.setTimeout(() => this.flash = false, 500);
        }
    }
});
