
// Return a cloned object that leaves all Vue bindings behind
export default function (obj) {
    return JSON.parse(JSON.stringify(obj));
}
