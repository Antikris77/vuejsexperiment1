
export default (arr) => {
    const newArr = arr.slice(0);
    return newArr.reverse();
};
