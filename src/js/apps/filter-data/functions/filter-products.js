
// Export an array of products filtered against activeFilters[]
// (clonify is no longer used here because I don't need a clone of the products here after all)
export default (products, activeFilters, filterTemplateData) => {
    return products.filter(product => {
        const results = activeFilters.map(activeFilter => {
            const filter = filterTemplateData.find(item => item.id === activeFilter.id);
            const value = activeFilter.value;
            const prop = product[filter.appliesTo];

            switch (filter.type) {
                case 'singleOption':
                    if (Array.isArray(prop)) {
                        return prop.indexOf(value) > -1;
                    } else {
                        return prop === value;
                    }
                case 'multiOption':
                    if (Array.isArray(prop)) {
                        // All entries in value[] are found in prop[]
                        const all = value.map(val => prop.indexOf(val) > -1);
                        return all.indexOf(false) === -1;
                    } else {
                        return value.indexOf(prop) > -1;
                    }
                case 'booleanOption':
                    return prop === value;
                default:
                    return false;
            }
        });

        // No active filter resulted in false
        return results.indexOf(false) === -1; 
    });
};
