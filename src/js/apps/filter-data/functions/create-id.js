
// Creates and returns a fairly random alphanumeric string
export default function (length=12) {
    const S4 = () => {
       return ((( 1 + Math.random())*0x10000) | 0).toString(16).substring(1);
    };
    return 'id-' + (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4()).substring(0, length - 1);
}
