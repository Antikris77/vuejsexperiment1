
/* 
    When passed a template literal, returns a function. This returned function can then be called with string arguments

    import tagTemplate from './tag-template.js';
    const template = tagTemplate `Hello ${0}!!`;
    console.log( template('Kristiaan') ); // => "Hello Kristiaan!"
*/
export default function (strings, ...keys) {
    return function (...values) {
        const dict = values[values.length - 1] || {};
        const result = [strings[0]];
        keys.forEach((key, i) => {
            const value = Number.isInteger(key) ? values[key] : dict[key];
            result.push(value, strings[i + 1]);
        });
        return result.join('');
    };
}
