
// Modifies products[] order based on sorter object
export default (products, sorter) => {
    products.sort(sorter.method);
    if (sorter.reversed) products.reverse();
};
