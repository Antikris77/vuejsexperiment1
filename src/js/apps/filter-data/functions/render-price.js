
export default (price) => {
    const p = parseFloat(price, 10);
    return p.toFixed(2); 
};
