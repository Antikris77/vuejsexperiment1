
import Vue from 'vue';
import store from './store';
import App from './components/app.vue';

import './mixins/def.js';
import './mixins/flash-component.js';

const app = new Vue({
    el: '#filter-data',
    store,
    render: h => h(App)
});
